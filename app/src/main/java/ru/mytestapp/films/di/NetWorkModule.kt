package ru.mytestapp.films.di

import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import ru.mytestapp.films.data.FilmsApi

@Module
class NetWorkModule {

    @Provides
    fun provideApi(
        okHttpClient: OkHttpClient
    ): FilmsApi {
        return Retrofit.Builder()
            .baseUrl("https://kinopoiskapiunofficial.tech/documentation/api/")
            .client(okHttpClient)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
            .create(FilmsApi::class.java)
    }

    @Provides
    fun provideokHttpClient(): OkHttpClient =
        OkHttpClient.Builder()
            .addInterceptor { chain ->
                val original = chain.request()
                val requestBuilder = original.newBuilder()
                    .header("X-API-KEY", "b57417f3-7d1f-45da-849a-165924124e7e")
                val request = requestBuilder.build()
                chain.proceed(request)
            }
            .build()

}