package ru.mytestapp.films.data

import io.reactivex.rxjava3.core.Single

interface NetworkRepository {

    fun getFilms(page:Int): Single<List<FilmResponse>>
}