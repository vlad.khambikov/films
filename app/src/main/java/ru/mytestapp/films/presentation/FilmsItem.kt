package ru.mytestapp.films.presentation

data class FilmsItem(val name: String, val rating: Double, val pathPoster: String)
