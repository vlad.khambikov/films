package ru.mytestapp.films.data

import io.reactivex.rxjava3.core.Single

class NetWorkRepositoryImpl private constructor(val api: FilmsApi) : NetworkRepository {
    override fun getFilms(page: Int): Single<List<FilmResponse>> {
        return api.getTopFilms(page = page)
            .map { response ->
                if (response.isSuccessful) {
                    response.body() ?: emptyList()
                } else {
                    throw Exception()
                }
            }
    }
}