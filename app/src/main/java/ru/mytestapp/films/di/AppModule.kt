package ru.mytestapp.films.di

import android.app.Application
import android.content.Context
import dagger.Module
import dagger.Provides
import javax.inject.Singleton


@Module
class AppModule {
    lateinit var _context: Context

    @Singleton
    @Provides
    fun provideContext(application: Application): Context {
        _context = application.applicationContext
        return _context
    }

}