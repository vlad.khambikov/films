package ru.mytestapp.films.di

import dagger.Component
import javax.inject.Singleton

@Component(modules = [AppModule::class, NetWorkModule::class])
@Singleton
interface AppComponent {
}