package ru.mytestapp.films.data

import io.reactivex.rxjava3.core.Single
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface FilmsApi {

    @GET("/api/v2.2/films/top")
    fun getTopFilms(
        @Query("type") type: String = "TOP_250_BEST_FILMS",
        @Query("page") page: Int
    ): Single<Response<List<FilmResponse>>>
}