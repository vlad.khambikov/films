package ru.mytestapp.films.presentation

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import ru.mytestapp.films.databinding.ItemFilmsBinding

class FilmsAdapter(val films: MutableList<FilmsItem> = mutableListOf()) :
    RecyclerView.Adapter<ItemViewHolder>() {

    fun setItems(films: MutableList<FilmsItem>) {
        this.films.clear()
        this.films.addAll(films)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder =
        ItemViewHolder(ItemFilmsBinding.inflate(LayoutInflater.from(parent.context), parent, false))

    override fun getItemCount(): Int = films.size

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        holder.bind(films[position])
    }
}

class ItemViewHolder(private val binding: ItemFilmsBinding) :
    RecyclerView.ViewHolder(binding.root) {
    fun bind(film: FilmsItem) {
        binding.name.text = film.name
        binding.rating.text = film.rating.toString()
    }
}