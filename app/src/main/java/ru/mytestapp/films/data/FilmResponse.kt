package ru.mytestapp.films.data

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

@Parcelize
data class FilmResponse(
    @SerializedName("nameRu") val nameRu: String,
    @SerializedName("ratingKinopoisk") val ratingKinopoisk: Double,
    @SerializedName("posterUrl") val posterUrl: String,
) : Parcelable
